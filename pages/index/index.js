// index.js
Page({
  data: {
    mode: '',
    monthVisible: false,
    month: '2021-09',
    monthText: '',
    // 用户登录名及密码
    loginid: '',
    password: '',

    // 工号对应的姓名
    mName: '',

    // 加班总时长
    totalOvertime: 0.00,
    // 加班图标
    overtimeIcon: 'happy',
    summary: '',

    // 指定选择区间起始值
    start: '2000-01-01 00:00:00',
    end: '2030-09-09 12:12:12',

    // 表格
    tableHeader: [
      {
        prop: 'date',
        width: 150,
        label: '日期',
        color: '#55C355'
      },
      {
        prop: 'check_in',
        width: 152,
        label: '上班时间'
      },
      {
        prop: 'check_out',
        width: 152,
        label: '下班时间'
      },
      {
        prop: 'lateTime',
        width: 110,
        label: '迟到时长'
      },
      {
        prop: 'overtime',
        width: 110,
        label: '加班时间'
      }
    ],
    stripe: true,
    border: true,
    outBorder: true,
    row: [],
    msg: '暂无数据'
  },
  showPicker(e) {
    const { mode } = e.currentTarget.dataset;
    let date = new Date(),
    year = date.getFullYear(),
    tmp_month = date.getMonth()+1
    this.setData({
      mode,
      [`${mode}Visible`]: true,
      start: (year-3)+'-'+tmp_month,
      end: year+'-'+tmp_month,
      month: year+'-'+tmp_month
    });
  },
  hidePicker() {
    const { mode } = this.data;
    this.setData({
      [`${mode}Visible`]: false,
    });
  },
  onConfirm(e) {
    const that = this;
    const { value } = e.detail;
    const { mode } = this.data;
    let totalOvertimeTmp = 0.00;
    let nameTmp = '';

    this.setData({
      [mode]: value,
      [`${mode}Text`]: value,
    });

    // 检查loginid与password是否为空
    if (!that.data.loginid || !that.data.password) {
      // 如果为空，则直接处理错误情况，而不是发送请求 
      wx.showModal({  
        title: '提示',  
        content: '登录凭证为空\n请检查您的登录凭证',  
        showCancel: false, // 不显示取消按钮  
        success: function (res) {  
          if (res.confirm) {  
            wx.navigateTo({  
              url: '/pages/self-info/self-info'  
            });
          }  
        }  
      });
      return;
    }

    this.hidePicker();
    
    wx.showLoading({
      title: '加载中',
    });
    // 两秒后更换title 
    setTimeout(function(){
      wx.hideLoading();
      // 稍后再显示新的加载提示框（设置一个极短的时间延迟来确保隐藏操作已经完成，例如100ms）
      setTimeout(function(){
        wx.showLoading({
          title: '请耐心等待',
        });
      }, 100);
    }, 1000);


    // 获取当前日期
    let formattedDate = getTodayDateStamp();
    // 使用 split 方法将字符串按照 '-' 分割成一个数组 ，随后获取月份
    let parts = e.detail.value.split('-');
    const month = parts[1]; 
    // key的结构为 ‘attData_进行查询的当天日期-查询的月份’
    const key = 'attData_' + formattedDate + "_" + month;
    console.log('key:' + key); 

    wx.getStorage({
      key: key,
      success: function (res) {
        setTimeout(function () {
          wx.hideLoading()
        }, 2000)
        that.setData({
          row: res.data.attData,
          totalOvertime: res.data.totalOvertime,
          overtimeIcon: res.data.overtimeIcon,
          summary: res.data.summary,
          mName: res.data.mName
        });
        console.log('本地获取考勤记录成功');
      },
      fail: function(res)  {
        console.log('本地获取考勤记录失败');

        // 从本地存储获取失败后，调用request进行查询
        wx.request({
          url: 'http://localhost:8080/get_date',
          data: {
              "date": e.detail.value,
              "loginid": that.data.loginid,
              "password": that.data.password
            },
          method: 'POST',
          header: {
              'content-type': 'application/json' //默认值
          },
          success: function(res) {
            setTimeout(function () {
              wx.hideLoading()
            }, 2000)
              console.log(res.data)
              const dataLength = res.data.data.length
              if (res.data != null){
                if (dataLength > 0){
                  const newData = res.data.data.map(item => {
                    const newItem = {  
                      ...item,  
                      date: item.data,
                      check_in: item.checkIn, 
                      check_out: item.checkOut,  
                      overtime: item.overtime,  
                      name: item.name  
                    };  
                
                    // 在这里累加overtime
                    totalOvertimeTmp += parseFloat(item.overtime) || 0; // 确保累加的是浮点数,如果转换失败，则默认为0  
                    nameTmp = item.name;
                    console.log(that.data.totalOvertime);
                    return newItem;  
                  });
  
                  // 如果查到key：attData不为空，则直接使用storage的数据
                  // 如果为空，则继续查询（查询在request之前）
  
                  let { overtimeIcon, summary } = getOvertimeIconAndSummary(totalOvertimeTmp); 
                  that.setData({
                    row: newData,
                    totalOvertime: totalOvertimeTmp.toFixed(2),
                    overtimeIcon: overtimeIcon,
                    summary: summary,
                    mName: nameTmp
                  });

                  // 获取当前日期的年和月  
                  const currentDate = new Date();
                  const currentMonth = currentDate.getMonth() + 1; // getMonth() 返回的是 0-11，所以需要 +1  
                  
                  // 只存储当月数据
                  if (month == ('0' + currentMonth)) {
                    wx.setStorage({
                      key: key,
                      // 临时保存查出来的数据，保存到当天12点
                      data: {
                        attData: newData, 
                        totalOvertime: that.data.totalOvertime, 
                        overtimeIcon: that.data.overtimeIcon, 
                        summary: that.data.summary,
                        mName: that.data.mName
                      },
                      success: function (){
                        console.log('本地存储考勤成功')
                      },
                      fail: (err) => {  
                        // 处理存储失败的情况
                        console.error('本地存储考勤失败');  
                      } 
                    })
                  }
                } else {
                  msg : '未获取到数据'
                  wx.showToast({  
                    title: '未获取到数据',  
                    icon: 'error' 
                  }); 
                }
  
              } else{
                console.error('没有获取到有效数据');
                wx.showToast({  
                  title: '没有获取到有效数据',  
                  icon: 'error' 
                }); 
              }
          },
          fail: function(error) {
            setTimeout(function () {
              wx.hideLoading()
            }, 2000);
            wx.showToast({  
              title: `${error.errMsg}`,  
              icon: 'error' 
            }); 
            console.error('请求失败：', error);
          }
        })
      }
    })
  },

  onColumnChange(e) {
    console.log('pick', e.detail.value);
  },
  
  /** 
   * 点击表格一行
   */
  onRowClick: function(e) {
    console.log('e: ', e)
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow(options) {
    const that = this
    wx.getStorage({
      key: 'userInfo',
      success: function (res) {
        console.log('页面开始显示即赋值{'+'loginid:'+res.data.loginid+'  password:'+res.data.password+'}')
        that.setData({
          loginid: res.data.loginid,
          password: res.data.password
        })
      }
    })
  }
})

function getOvertimeIconAndSummary(totalOvertimeTmp) {
  // 在不同加班时间切换不同的Icon
  let overtimeIcon = 'happy';
  let summary = '';
  if (totalOvertimeTmp > 5 && totalOvertimeTmp < 10) {
    overtimeIcon = 'serenity'
  } else if(totalOvertimeTmp >= 10 && totalOvertimeTmp < 20){
    overtimeIcon = 'sinister-smile'
  } else if (totalOvertimeTmp >= 20 && totalOvertimeTmp < 30) {
    overtimeIcon = 'emo-emotional'
  } else if (totalOvertimeTmp >= 30 && totalOvertimeTmp < 40){
    overtimeIcon = 'cry-loudly'
  } else if (totalOvertimeTmp >= 30 && totalOvertimeTmp < 40){
    overtimeIcon = 'depressed'
  } else if (totalOvertimeTmp >= 40 && totalOvertimeTmp < 50) {
    overtimeIcon = 'speechless-1'
  } else if (totalOvertimeTmp >= 50 && totalOvertimeTmp < 60) {
    overtimeIcon = 'uncomfortable-1'
  } else if (totalOvertimeTmp >= 60 && totalOvertimeTmp < 100) {
    overtimeIcon = 'crack'
  } else if (totalOvertimeTmp >= 100 && totalOvertimeTmp < 200) {
    overtimeIcon = 'flip-smiling-face'
  }else if (totalOvertimeTmp >= 200) {
    overtimeIcon = 'error-circle-filled'
    summary = '不想活啦!!!'
  }

  // 返回一个对象，包含overtimeIcon和summary  
  return {  
    overtimeIcon: overtimeIcon,  
    summary: summary  
  }; 
}

// 获取当前日期对应的零点时间戳  
function getTodayDateStamp() {  
  // 获取当前日期 
  const currentDate = new Date();  
  // 格式化日期为 "yyyy-MM-dd"  
  const formattedDate = `${currentDate.getFullYear()}-${('0' + (currentDate.getMonth() + 1)).slice(-2)}-${('0' + currentDate.getDate()).slice(-2)}`;
  return formattedDate;
}
