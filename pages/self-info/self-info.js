// pages/self-info/self-info.js
import Toast from 'tdesign-miniprogram/toast/index';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    loginid: '',
    password: '',
    oldLoginid: ''
  },

  

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    const that = this
    wx.getStorage({
      key: 'userInfo',
      success: function (res) {
        that.setData({
          loginid: res.data.loginid,
          password: res.data.password,
          oldLoginid: res.data.oldLoginid
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },

  showSuccessToast(){
    wx.setStorage({
      key: 'userInfo',
      data: {loginid: this.data.loginid, password: this.data.password},
      success: function (){
        console.log('设置成功 {loginid:'+this.data.loginid+'  password:'+this.data.password+'}')

        // oldLoginid为修改前的loginid，当修改loginid后，清楚考勤数据本地缓存
        if (this.data.loginid != this.data.oldLoginid) {
          wx.getStorageInfo({  
            success: function(res) {  
              const prefix = 'attData_';
        
              // 遍历所有keys  
              res.keys.forEach(function(key) {  
                if (key.startsWith(prefix)) {
                  wx.removeStorage({  
                    key: key,  
                    success: function() {  
                      console.log('成功删除过期数据：', key);  
                    },  
                    fail: function(err) {  
                      console.error('删除过期数据失败：', err);  
                    }  
                  }); 
                }  
              });  
            },  
            fail: function(err) {  
              console.error('获取存储信息失败：', err);  
            }  
          });  
        }

        wx.showToast({  
          title: '保存成功',  
          icon: 'success',  
          duration: 2000 
        }); 
        setTimeout(() => {  
          wx.navigateBack();  
        }, 2000);
      },
      fail: (err) => {  
        // 处理存储失败的情况  
        wx.showToast({  
          title: '保存失败',  
          icon: 'error',  
          duration: 2000 
        }); 
        console.error('设置失败', err);  
      } 
    })
    // Toast({
    //   context: this,
    //   selector: '#t-toast',
    //   message: '保存成功',
    //   theme: 'success',
    //   direction: 'column'
    // })
    
  }
  
})