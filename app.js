// app.js  
App({  
  globalData: {  
    // 你的全局数据  
  },  
  
  onLaunch: function() {  
    this.checkAndRemoveExpiredData();  
  },  
  
  checkAndRemoveExpiredData: function() {  
    // 获取当前日期对应的零点时间戳  
    function getTodayDateStamp() {  
      // 获取当前日期 
      const currentDate = new Date();  
      // 格式化日期为 "yyyy-MM-dd"  
      const formattedDate = `${currentDate.getFullYear()}-${('0' + (currentDate.getMonth() + 1)).slice(-2)}-${('0' + currentDate.getDate()).slice(-2)}`;
      return formattedDate;
    }  
  
    // 获取存储信息  
    wx.getStorageInfo({  
      success: function(res) {  
        const prefix = 'attData_';  
        const todayDateStamp = getTodayDateStamp();  
  
        // 遍历所有keys  
        res.keys.forEach(function(key) {  
          if (key.startsWith(prefix)) {  
            // 提取日期部分  
            let mDateStr = key.slice(prefix.length); 
            let datePart = mDateStr.split('-');
            const dateStr = datePart[1];
  
            // 检查日期时间戳是否小于今天零点的时间戳
            if (dateStr != todayDateStamp) {  
              wx.removeStorage({  
                key: key,  
                success: function() {  
                  console.log('成功删除过期数据：', key);  
                },  
                fail: function(err) {  
                  console.error('删除过期数据失败：', err);  
                }  
              });  
            }  
          }  
        });  
      },  
      fail: function(err) {  
        console.error('获取存储信息失败：', err);  
      }  
    });  
  }  
});